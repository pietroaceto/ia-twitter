import pandas as pd
import sklearn as sk
import os
from scipy.io import arff

os.chdir('/Volumes/Macintosh HD/ia-dm/iris-dataset')

# READ FROM ARFF
# all_data = arff.loadarff('iris.arff')
# data = pd.DataFrame(all_data[0])
# data1 = pd.DataFrame(all_data[1])
# print(data)
# print(data1)

# READ FROM CSV
data = pd.read_csv("/Volumes/Macintosh HD/ia-dm/iris-dataset/iris_training.csv", header=None)
print(data)